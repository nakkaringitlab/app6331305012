import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Welcome To",
      home: MyHomePage(),
      theme: ThemeData(primarySwatch: Colors.red),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int number = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          "Welcome To MFU Wallet",
          style: TextStyle(color: Colors.white, fontSize: 15),
        ),
        leading: IconButton(
          onPressed: () {},
          icon: Icon(Icons.person_rounded),
        ),
        actions: [
          IconButton(
            onPressed: () {},
            icon: Icon(Icons.add_alert_sharp),
          ),
          IconButton(
            onPressed: () {},
            icon: Icon(Icons.more_horiz),
          ),
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            backgroundColor: Colors.red,
            icon: Icon(Icons.home),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.wifi_protected_setup),
            label: 'Transfer',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.monetization_on_sharp),
            label: 'Banking',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.qr_code_2_sharp),
            label: 'Scan/MyQR',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.more_horiz),
            label: 'More',
          ),
        ],
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              //first
              color: Colors.red,
              height: 32.0,
              width: 300.0,
              child: Text( //test
                " | Quick Balance",
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 17.0,
                ),
              ),
            ),
            Container(
              color: Colors.red,
              height: 40.0,
              width: 300.0,
              child: Text(
                "Add account or card to instantily check balance",
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 12,
                ),
              ),
            ),
            Container(
              color: Colors.white,
              height: 40.0,
              width: 300.0,
            ),
            Container(
              color: Colors.red,
              height: 32.0,
              width: 300.0,
              child: Text(
                //second
                " | Quick Balance",
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 17.0,
                ),
              ),
            ),
            Container(
              color: Colors.red,
              height: 40.0,
              width: 300.0,
              child: Text(
                "Add account or card to instantily check balance",
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 12,
                ),
              ),
            ),
            Container(
              color: Colors.white,
              height: 40.0,
              width: 300.0,
            ),
            Container(
              color: Colors.red,
              height: 32.0,
              width: 300.0,
              child: Text(
                " | My favorites",
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 17.0,
                ),
              ),
            ),
            Container(
              color: Colors.red,
              height: 40.0,
              width: 300.0,
              child: Text(
                "Add favorited transation to Home page",
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 12,
                ),
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        child: Icon(Icons.chat),
      ),
    );
  }
}
